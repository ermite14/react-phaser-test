import { removeByCondition } from '../CopyUtils';

type IdType = string | number;

type IdEntity<ID extends IdType> = {
  id?: ID;
};

export type Entity<ID extends IdType, D extends IdEntity<ID>> = IdEntity<ID> &
  {
    data: D,
  };

export default class EntityCache<ID extends IdType, T extends Entity<ID, D>, D extends IdEntity<ID>> {
  protected _entities: T[] = [];

  constructor(
    private _entityFactory: (data: D) => T
  ) {}

  get = (id: ID) => {
    for (const e of this._entities) {
      if (e.id === id) return e;
    }
    return undefined;
  }

  getByIndex = (ind: number) => {
    if (ind < this._entities.length && ind >= 0) return this._entities[ind];
    return undefined;
  }

  firstWhere = <K extends keyof T>(key: K, value: T[K]) => {
    for (const e of this._entities) {
      if (e[key] === value) return e;
    }
    return undefined;
  }

  deleteById = (id: ID) => {
    removeByCondition(this._entities, entity => entity.id === id);
  }
  delete = (value: T) => {
    removeByCondition(this._entities, entity => entity === value);
  }

  fromData = (data: D, forceSetData = false) => this.fromArray([data], forceSetData)[0];
  /**
   * Функция создания моделей из массива данных
   * @param {D[]} data - массив данных
   * @param forceSetData - если выставлено, то вызывает функцию setData
   * вне зависимости от того isFull сущность или нет.
   * В обычном режиме setData вызывается только при !isFull сущности
   * @returns {T[]} - список моделей, соответствующих данным
   */
  fromArray = (data: D[], forceSetData = false) => {
    const newEntities:T[] = [];
    for (const entityData of data) {
      const id = entityData.id;
      let entity = id ? this.get(id) : undefined;
      if (!entity) {
        entity = this._entityFactory(entityData);
        this._entities.push(entity);
      } else {
        if (forceSetData) {
          // entity.setData(entityData);
        }
      }
      newEntities.push(entity);
    }
    return newEntities;
  }

  /**
   * Аналог fromData, но удаляет лишние данные из массива и синхронизует с внешними данными (даже если данные не full)
   * @param {DGalleryElementAny[]} data
   */
  sync = (data: D[]) => {
    this._entities = this.fromArray(data, true);
  }

  /*
    Array functions proxies
   */
  map = <S>(callback: (item: T, index: number) => S) => this._entities.map(callback);
  filter = (callback: (item: T) => boolean) => this._entities.filter(callback);
  forEach = (callBack: (item: T) => void) => { this._entities.forEach(callBack); };
  sort = (func: (a: T, b: T) => number) => { this._entities = this._entities.sort(func); };
  get length() { return this._entities.length; }

  get entityData() {
    return this.map(entity => entity.data);
  }

  removeAll = () => {
    this._entities = [];
  }
}
