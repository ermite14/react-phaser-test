import Phaser from 'phaser';
import { WindowManager } from '../app/WindowManager';

export class TestScene extends Phaser.Scene {
  private radius = 70;
  // private tileSpriteDuration = 6;
  private tileSprite!: Phaser.GameObjects.TileSprite;
  private spriteCountText!: Phaser.GameObjects.Text;
  private spriteCount = 0;
  // private tilesScale = 0.009;

  preload = () => {
    this.load.image('unit504_R1', 'http://mira-play.com/fantasy/units/unit651_L1.png');
    this.load.image('16-3', 'image/16-3.jpg');
  }

  create = () => {
		
		const res = this.addRandomSprite();
		res.setScale(0.45);
		res.displayWidth = Math.floor(res.displayWidth);
		res.displayHeight = Math.floor(res.displayHeight);
		console.log(res);

		this.cameras.main.setRenderToTexture();
		return;
		
    const { width, height } = this.game.canvas;
    const cX = Math.floor(width / 2);
    const cY = Math.floor(height / 2);
		
		// this.cameras.main.setZoom(0.45);

    this.tileSprite = this.add.tileSprite(cX, cY, width, height, '16-3')
      .setOrigin(0.5);

    this.spriteCountText = this.add.text(10, height - 35, 'Sprite count: 0', { fontSize: 30 })
      .setDepth(11);

    this.addCircle(cX, cY, 'Open window', () => {
      const win = WindowManager.testWin();
      win.show();
    });
		
		
		console.log(res);

    this.addCircle(cX - 140, cY, 'Open fullscreen', () => {
      const scale = this.scale;
      if (scale.isFullscreen) this.scale.stopFullscreen();
      else scale.startFullscreen();

      const { width, height } = screen;
      // меняем размер канваса
      scale.resize(width, height);
      // меняем размер внутри игры
      // scale.setGameSize(width, height);
      // меняем размер камеры
      this.cameras.resize(width, height);

      this.tileSprite.setSize(this.cameras.main.width, Math.min(this.cameras.main.height, 700))
        .setOrigin(.5);
    });

    this.addCircle(cX + 140, cY, 'Add Sprite', this.addRandomSprite);
  }

  private addRandomSprite = () => {
    const rnd = this.randomPosition;
    const targets = this.add.sprite(rnd.x, rnd.y, 'unit504_R1');
    targets.setOrigin(.5);

    /*this.add.tween({
      targets, x, y,
      repeat: -1,
      yoyo: true,
      duration: this.randomInteger(200, 2000),
    });*/

    this.spriteCount += 1;
    if (this.spriteCountText) this.spriteCountText.text = 'Sprite count: ' + this.spriteCount;
		return targets;
  }

  private get randomPosition() {
    const { width, height } = this.cameras.main;
    const x = this.randomInteger(0, width);
    const y = this.randomInteger(0, height);
    return { x, y };
  }

  private randomInteger = (min: number, max: number) => {
    const rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }

  private addCircle = (x: number, y: number, text: string, event: Function) => {
    const circle = this.add.circle(x, y, this.radius, 0xCCCCCC);
    circle.setInteractive({ useHandCursor: true });
    circle.on('pointerup', event);
    circle.setDepth(10);
    this.add.text(x, y, text, { color: '#000' })
      .setOrigin(0.5)
      .setDepth(11);
  }

  update(time: number, delta: number): void {
    /*const ts = this.tileSprite;
    ts.setTilePosition(ts.tilePositionX + this.tileSpriteDuration, ts.tilePositionY);*/

    // if (ts.scaleX > 1.2) this.tilesScale = -0.009;
    // if (ts.scaleX < 1.01) this.tilesScale = 0.009;

    // ts.setScale(ts.scaleX + this.tilesScale);
  }
}
