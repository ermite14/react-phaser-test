import cloneDeep from 'clone-deep';

export const copyExcept = <T>(source: T, destination: T, exceptKeys?: (keyof T)[]) => {
  for (const k in source) {
    if (!source.hasOwnProperty(k)) continue;
    if (!destination.hasOwnProperty(k)) continue;
    if (exceptKeys && exceptKeys.indexOf(k) !== -1) continue;
    destination[k] = source[k];
  }
};

export const removeByCondition = <T>(source: T[], condition: (item: T) => boolean) => {
  const len = source.length;
  for (let i = len - 1; i >= 0; i -= 1) {
    if (condition(source[i])) source.splice(i, 1);
  }
};

export const cloneEntity = <
  T,
  D extends { id: number },
  C extends new (data: D) => T,
  >(entityClass: C, data: D, newId?: number): T => {
  const newData = cloneDeep(data);
  if (newId) newData.id = newId;
  return new entityClass(newData);
};
