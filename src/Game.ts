import Phaser from 'phaser';
import { TestScene } from './canvas/TestScene';

export class Game extends Phaser.Game {
  constructor(width: number, height: number) {
    super({
      type: Phaser.CANVAS,
      scale: {
        height,
        width,
        parent: 'canvas',
				mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        autoRound: true,
				
      render: {
        mipmapFilter: 'LINEAR_MIPMAP_LINEAR',
      },
      },
    } as any);

    this.scene.add('Test', TestScene, true);
  }
}
