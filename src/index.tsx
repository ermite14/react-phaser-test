import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'moment/locale/ru';
import * as moment from 'moment';
import { Game } from './Game';

moment.locale('ru');
import App from './app/App';

const body = document.getElementsByTagName('body')[0];

// базовая величина, чтобы указать фазеру точный размер игры
const baseHeight = body.offsetHeight;
// Базовая ширина
const baseWidth = body.offsetWidth;

export const baseScreenParam = {
  main: { width: baseWidth, height: baseHeight },
  fullscreen: { width: screen.width, height: screen.height },
};

export const game = new Game(baseWidth, baseHeight);

const root = document.getElementById('react') as HTMLElement;

export const ReactApp = React.createElement(App);
const initDom = (): void => {
  ReactDOM.render(
    <>{ReactApp}</>,
    root
  );
};

initDom();
game.scale.fullscreenTarget = document.getElementById('root');
