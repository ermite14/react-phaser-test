import * as React from 'react';
import BigWindow from './BigWindow';

type WindowContentProps = { title: string };

export class WindowContent extends React.Component<WindowContentProps> {
  render() {
    return(
      <div style={{ padding: '15px 5px 10px 10px' }}>
        <div style={{ textAlign: 'center', position: 'absolute', width: 605, marginLeft: 80 }}>{this.props.title}</div>
        <div
          style={{
            background: 'url(./image/btCloseWin.png)',
            float: 'right',
            cursor: 'pointer',
            width: 35,
            height: 32,
            zIndex: 100000,
          }}
          onClick={BigWindow.close}
        />
        <div style={{ display: 'flex', color: '#fff', marginTop: 25 }}>
          Содержимое окна
        </div>
      </div>
    );
  }
}
