import * as React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import { WindowContent } from './WindowContent';

@observer
export default class BigWindow extends React.Component {
  @observable private static _show = false;

  static show = () => BigWindow._show = true;
  static close = () => BigWindow._show = false;

  render() {
    const display = BigWindow._show ? '' : 'none';
    const width = 765;
    const height = 453;
    return (
      <div>
        <div
          style={{
            display, width, height,
            background: 'url(./image/window.png)',
            clear: 'both',
          }}
        >
          <div style={{ width, height, background: 'url(./image/windowFrwd.png)' }}>
            <WindowContent title={'Название окна'} />
          </div>
        </div>
      </div>
    );
  }
}
