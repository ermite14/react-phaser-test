declare module 'clone-deep' {
  const cloneDeep: <T>(a: T) => T;
  export = cloneDeep;
}
